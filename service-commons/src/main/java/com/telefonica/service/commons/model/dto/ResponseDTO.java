package com.telefonica.service.commons.model.dto;

import com.telefonica.service.commons.enums.EResponseType;

public class ResponseDTO<T> {
	
	private T serviceResponse;
	private EResponseType type;
	private String message;
	private String code;

}
