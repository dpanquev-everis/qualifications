package com.telefonica.service.commons.enums;

public enum EResponseType {

	SUCCESS,
	ERROR,
	NO_CONTENT,
	NO_FOUND
}
