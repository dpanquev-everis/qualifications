package com.telefonica.service.commons.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telefonica.service.commons.model.entity.Qualification;

@Repository
public interface IQualificationRepository extends JpaRepository<Qualification, Long> {

}
