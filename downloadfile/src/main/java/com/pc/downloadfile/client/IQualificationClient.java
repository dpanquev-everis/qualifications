package com.pc.downloadfile.client;

import org.springframework.data.repository.CrudRepository;

import com.pc.downloadfile.model.QualificationResponse;

public interface IQualificationClient extends CrudRepository<QualificationResponse, Integer> {}
