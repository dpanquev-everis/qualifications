package com.pc.downloadfile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DownloadfileApplication {

	public static void main(String[] args) {
		SpringApplication.run(DownloadfileApplication.class, args);
	}

}
