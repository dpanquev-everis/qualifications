package com.pc.downloadfile.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;

import com.pc.downloadfile.client.IQualificationClient;
import com.pc.downloadfile.model.CreateData;
import com.pc.downloadfile.model.QualificationResponse;
import com.pc.downloadfile.service.IQualificationService;

@Service
public class QualificationService implements IQualificationService {
	
	@Autowired
	IQualificationClient qualificationClient;
		
	/*
	 * Metodo que permite descargar un archivo csv con tres registros y almacenar el csv en la ruta C:\reports\ con nombre csvdata.csv. 
	 */
	@Override
	public File buildFileCSV() {
		List<QualificationResponse> responseList = new ArrayList();
		CreateData qualification1 = new CreateData();
		responseList.add(qualification1.createQualificationResponse());
		responseList.add(qualification1.createQualificationResponse());
		responseList.add(qualification1.createQualificationResponse());
		
		StringBuilder fileContent = new StringBuilder("id_qualification,channel,qualification\n");
		responseList.forEach(csv->{
			fileContent.append(csv.getId_qualification()).append(",").append(csv.getChannel()).append(",").append(csv.getQualification()).append("\n");	
		});
		String filename= "C:\\reports\\csvdata.csv";
		try {
			FileWriter fileWriter = new FileWriter(filename);
			fileWriter.write(fileContent.toString());
			fileWriter.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		File file = new File(filename);
		return file;
	}
	
}
