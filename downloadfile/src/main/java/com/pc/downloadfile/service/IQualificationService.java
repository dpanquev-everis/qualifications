package com.pc.downloadfile.service;

import java.io.File;
import java.util.List;

import com.pc.downloadfile.model.QualificationResponse;

public interface IQualificationService {
	public File buildFileCSV();
}
