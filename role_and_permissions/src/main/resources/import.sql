INSERT INTO usuario(nombre,apellido,usuario) values ('Pepito','Perez','pperez');
INSERT INTO usuario(nombre,apellido,usuario) values ('Rodrigo','Montano','rmontano');
INSERT INTO usuario(nombre,apellido,usuario) values ('Lilia','Sanchez','lsanchez');

INSERT INTO rol(descripcion,usuario_id) VALUES('USER',1);
INSERT INTO rol(descripcion,usuario_id) VALUES('ADMIN',2);

INSERT INTO permiso(permiso_type,rol_id) VALUES('app digital',1);
INSERT INTO permiso(permiso_type,rol_id) VALUES('app digital',2);
INSERT INTO permiso(permiso_type,rol_id) VALUES('chatbot',2);
INSERT INTO permiso(permiso_type,rol_id) VALUES('canal 3',1);
INSERT INTO permiso(permiso_type,rol_id) VALUES('canal 4',1);
INSERT INTO permiso(permiso_type,rol_id) VALUES('canal 4',2);
