package com.telefonica.pcspringsecurity.repository;

import org.springframework.data.repository.CrudRepository;
import com.telefonica.pcspringsecurity.model.Rol;

public interface RolRepository extends CrudRepository<Rol, Integer>{
}
