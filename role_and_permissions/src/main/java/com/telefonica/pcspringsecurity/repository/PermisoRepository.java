package com.telefonica.pcspringsecurity.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.telefonica.pcspringsecurity.model.Permiso;

@Repository
public interface PermisoRepository extends JpaRepository<Permiso, Integer>{
}
