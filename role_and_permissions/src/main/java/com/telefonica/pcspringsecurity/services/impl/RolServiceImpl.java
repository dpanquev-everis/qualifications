package com.telefonica.pcspringsecurity.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.pcspringsecurity.model.Rol;
import com.telefonica.pcspringsecurity.repository.RolRepository;
import com.telefonica.pcspringsecurity.services.RolService;

@Service
public class RolServiceImpl implements RolService{
	
	@Autowired
	private RolRepository rolRepository;
	
	@Override
	public void saveRol(Rol rol) {
		rolRepository.save(rol);
	}

	@Override
	public List<Rol> getAllRol() {
		return (List<Rol>) rolRepository.findAll();
	}

}
