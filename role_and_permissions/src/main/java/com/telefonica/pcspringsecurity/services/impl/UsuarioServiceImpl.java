package com.telefonica.pcspringsecurity.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.pcspringsecurity.model.Usuario;
import com.telefonica.pcspringsecurity.repository.UsuarioRepository;
import com.telefonica.pcspringsecurity.services.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService{
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Override
	public void saveUser(Usuario usuario) {
		usuarioRepository.save(usuario);
	}

	@Override
	public List<Usuario> getAllUser() {
		return (List<Usuario>) usuarioRepository.findAll();
	}

	@Override
	public Usuario accessUser(String username) {
		return usuarioRepository.findByUsuario(username);
	}

}
