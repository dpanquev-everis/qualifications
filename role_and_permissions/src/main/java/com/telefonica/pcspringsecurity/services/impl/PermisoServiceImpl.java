package com.telefonica.pcspringsecurity.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.pcspringsecurity.model.Permiso;
import com.telefonica.pcspringsecurity.repository.PermisoRepository;
import com.telefonica.pcspringsecurity.services.PermisoService;

@Service
public class PermisoServiceImpl implements PermisoService {
	
	@Autowired
	private PermisoRepository permisoRepository;
	
	@Override
	public void savePermniso(Permiso permiso) {
		permisoRepository.save(permiso);
	}

	@Override
	public List<Permiso> getAllPermiso() {
		return (List<Permiso>) permisoRepository.findAll();
	}
	
	
}
