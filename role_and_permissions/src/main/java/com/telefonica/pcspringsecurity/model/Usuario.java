package com.telefonica.pcspringsecurity.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;


@Entity
@Table(name="usuario")
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class Usuario {
	
	@Id
	@Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="nombre")
	private String nombre;
	
	@Column(name="apellido")
	private String apellido;
	
	@Column(name="usuario")
	private String usuario;
	
	@JsonIgnoreProperties(value = {"usuario"}, allowSetters = true)
	@OneToMany(fetch=FetchType.LAZY , cascade = CascadeType.ALL, mappedBy = "usuario")
	private List<Rol> roles;
		
}
