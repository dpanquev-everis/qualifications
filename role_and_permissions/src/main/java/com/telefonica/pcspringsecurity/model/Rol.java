package com.telefonica.pcspringsecurity.model;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Entity
@Table(name="rol")
@Data
public class Rol {
	
	@Id
	@Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="descripcion")
	private String descripción;
	
	@JsonIgnoreProperties(value = {"roles"})
	@ManyToOne
    @JoinColumn(name="usuario_id", nullable = false, updatable = false)
	private Usuario usuario;
	
	@JsonIgnoreProperties(value = {"rol"}, allowSetters = true)
	@OneToMany(fetch=FetchType.LAZY,cascade = CascadeType.ALL, mappedBy = "rol")
	private List<Permiso> permisos;
}
