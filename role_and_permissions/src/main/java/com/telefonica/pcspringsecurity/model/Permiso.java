package com.telefonica.pcspringsecurity.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Entity
@Table(name="permiso")
@Data
public class Permiso {
	
	@Id
	@Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="permiso_type")
	private String permiso_type;
	
	@JsonIgnoreProperties(value = {"permisos"})
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="rol_id", nullable = false, updatable = false)
	private Rol rol;

}
