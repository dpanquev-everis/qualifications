package com.telefonica.pcspringsecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PcspringsecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(PcspringsecurityApplication.class, args);
	}

}
