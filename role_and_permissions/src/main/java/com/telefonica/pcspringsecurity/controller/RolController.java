package com.telefonica.pcspringsecurity.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.telefonica.pcspringsecurity.model.Rol;
import com.telefonica.pcspringsecurity.services.RolService;

@RestController
@RequestMapping("/rol")
public class RolController {
	@Autowired
	private RolService rolService;
	
	@PostMapping("/save")
	public ResponseEntity<Object> saveRol(@RequestBody Rol rol) {
		rolService.saveRol(rol);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@GetMapping("/getAll")
	public ResponseEntity<List<Rol>> getAllUser(){
		List<Rol> rolList = rolService.getAllRol();
		return new ResponseEntity<>(rolList, HttpStatus.OK);
	}
}
