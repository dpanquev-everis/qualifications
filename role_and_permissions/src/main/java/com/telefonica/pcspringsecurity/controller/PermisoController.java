package com.telefonica.pcspringsecurity.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telefonica.pcspringsecurity.model.Permiso;
import com.telefonica.pcspringsecurity.model.Rol;
import com.telefonica.pcspringsecurity.services.PermisoService;
import com.telefonica.pcspringsecurity.services.RolService;

@RestController
@RequestMapping("/permiso")
public class PermisoController {
	@Autowired
	private PermisoService permisoService;
	
	@PostMapping("/save")
	public ResponseEntity<Object> savePermission(@RequestBody Permiso permiso) {
		permisoService.savePermniso(permiso);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@GetMapping("/getAll")
	public ResponseEntity<List<Permiso>> getAllPermission(){
		List<Permiso> permisoList = permisoService.getAllPermiso();
		return new ResponseEntity<>(permisoList, HttpStatus.OK);
	}
}
