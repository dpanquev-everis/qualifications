package com.telefonica.pcspringsecurity.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telefonica.pcspringsecurity.model.Usuario;
import com.telefonica.pcspringsecurity.services.UsuarioService;

@RestController
@RequestMapping("/user")
public class UsuarioController {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@PostMapping("/save")
	public ResponseEntity<Object> saveUser(@RequestBody Usuario usuario) {
		usuarioService.saveUser(usuario);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@GetMapping("/getAll")
	public ResponseEntity<List<Usuario>> getAllUser(){
		List<Usuario> usuarioList = usuarioService.getAllUser();
		return new ResponseEntity<>(usuarioList, HttpStatus.OK);
	}
	
	@GetMapping("/user-consult-access/usuario/{user}")
	public ResponseEntity<Usuario> getUserAccess(@PathVariable String user){
		Usuario us = usuarioService.accessUser(user);
		return new ResponseEntity<>(us,HttpStatus.OK);
	}
}
