package com.telefonica.poc.model.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.telefonica.service.commons.model.entity.Qualification;

public interface IQualificationService {
	
	public Page<Qualification> findAll(Pageable pageable);

}
