package com.telefonica.poc.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.telefonica.poc.exception.NotFoundException;
import com.telefonica.service.commons.model.entity.Qualification;
import com.telefonica.service.commons.model.repository.IQualificationRepository;

/**
 * Clase base capa de negocio
 *
 * @author dpanquev
 * @version 2021-07-14
 */
@Service
public class QualificationServiceImpl implements IQualificationService{
	
	@Autowired
	private IQualificationRepository qualificationRepository;

	
	/**
	 * Metodo para consultar data en H2 paginada
	 * @param pageable, corresponde a la configuración inicial proveniente del restController
	 * @return
	 * */
	@Override
	public Page<Qualification> findAll(Pageable pageable) {
		 Page<Qualification> qualPage = null;
		 qualPage = qualificationRepository.findAll(pageable);
		 if(qualPage.isEmpty()) {
			 throw new NotFoundException("No se encuentra información");
		 }
		return qualPage;
	}

}
