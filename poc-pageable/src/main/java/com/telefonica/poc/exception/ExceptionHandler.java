package com.telefonica.poc.exception;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
public class ExceptionHandler {

	@org.springframework.web.bind.annotation.ExceptionHandler(NotContentException.class)
	public ResponseEntity<?> notContentdException(Exception e) {
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(e.getMessage());
	}

	@org.springframework.web.bind.annotation.ExceptionHandler(BadRequestException.class)
	public ResponseEntity<?> batRequestException(Exception e) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
		
	}

}
