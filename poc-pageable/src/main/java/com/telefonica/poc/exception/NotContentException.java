package com.telefonica.poc.exception;

public class NotContentException extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotContentException() {
		
	}
	
	public NotContentException(String message) {
		super(message);
	}
	

}
