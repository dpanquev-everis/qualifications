package com.telefonica.poc.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telefonica.poc.model.service.IQualificationService;
import com.telefonica.service.commons.model.entity.Qualification;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

/**
 * Clase base para el trabajo con api rest
 *
 * @author dpanquev
 * @version 2021-07-14
 */
@RestController
@RequestMapping("/api/VI")
public class QualificationController {

	@Autowired
	private IQualificationService qualificationService;

	/**
	 * Metodo para traer la información de la base de datos H2
	 * @param page, corresponde al número de págin a consultar 
	 * @param numData, corresponde a la cantidad de datos por página consultada
	 * @return
	 */
	@GetMapping("/calificaciones/page/{page}/numdata/{numData}")
	public Page<Qualification> index(@PathVariable Integer page,@PathVariable Integer numData) {
		return qualificationService.findAll(PageRequest.of(page, numData));
	}

}
