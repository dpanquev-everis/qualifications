package com.telefonica.poc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan({"com.telefonica.service.commons.model.entity"})
@EnableJpaRepositories("com.telefonica.service.commons.model.repository")
public class PocPageableApplication {

	public static void main(String[] args) {
		SpringApplication.run(PocPageableApplication.class, args);
	}

}
